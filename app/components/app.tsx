import * as React from "react"
import { connect } from "react-redux"
import { bindActionCreators, ActionCreatorsMapObject } from "redux"

import { appActionsMap } from "../actions/index"

interface IState {
    
}

interface IProps {
    appState: any,
    appActions: any
}

class App extends React.Component<IProps, IState> {

    constructor(props: any) {
        super(props);
    }
    
    render() {
        return <div>HelloWorld</div>;
    }
}

function mapStateToProps(state: any) {
    return {
        appState: state
    }
}

function mapDispatchToProps(dispatch: any) {
    return {
        appActions: bindActionCreators(appActionsMap, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);