import { ActionCreator, Action, ActionCreatorsMapObject } from "redux"
import { Dispatch } from "react-redux";

export const exampleAction: ActionCreator<Action> = (value: string) => ({
    type: "example",
    payload: {
      value: value
    }
  })

export const appActionsMap: ActionCreatorsMapObject = {
  exampleAction: exampleAction
}

export default appActionsMap;