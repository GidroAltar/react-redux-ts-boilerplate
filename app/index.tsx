import * as React from "react"
import * as ReactDOM from "react-dom"
import { createStore, applyMiddleware  } from "redux"
import { Provider } from "react-redux"
import thunk from "redux-thunk"
import { composeWithDevTools } from 'redux-devtools-extension'
import mainReducer from "./reducers/index"
import App from "./components/app"

let initialState = {
  inputValue: ""
};

const store = createStore(mainReducer, initialState, composeWithDevTools(applyMiddleware(thunk)));

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("app")
);
